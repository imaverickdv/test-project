export function isUndefined(value: any) {
    return value === undefined;
}

export function toJson(value: any) {
    return JSON.stringify(value);
}