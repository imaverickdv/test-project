import { ConfigurableModuleBuilder } from '@nestjs/common';

export interface UsersModuleOptions {
    debug?: boolean;
}

export const { ConfigurableModuleClass, MODULE_OPTIONS_TOKEN, OPTIONS_TYPE } =
    new ConfigurableModuleBuilder<UsersModuleOptions>({
        moduleName: 'Users'
    }).build();