import { AbstractModel } from "@app/common";

export class UserModel extends AbstractModel {
    public id: number;
    public name: string;
    public token: string;
}