import { DynamicModule, Global, Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { ConfigurableModuleClass, OPTIONS_TYPE } from './users.definition';
import { UsersRepository } from "@app/users/users.repository";

@Global()
@Module({
    providers: [UsersService, UsersRepository],
    exports: [UsersService]
})
export class UsersModule extends ConfigurableModuleClass {
    static register(options: typeof OPTIONS_TYPE): DynamicModule {
        return {
            ...super.register(options)
        }
    }
}
