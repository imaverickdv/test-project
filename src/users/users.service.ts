import { Inject, Injectable, Logger } from '@nestjs/common';
import { MODULE_OPTIONS_TOKEN, UsersModuleOptions } from './users.definition';
import { capitalizeFirstLetter, generateRandomString } from "@common/lib/utils";
import { UsersRepository } from "@app/users/users.repository";
import { UserModel } from "@app/users/user.model";

@Injectable()
export class UsersService {
    private logger = new Logger(UsersService.name);

    constructor(
        @Inject(MODULE_OPTIONS_TOKEN) private options: UsersModuleOptions,
        private usersRepository: UsersRepository,
    ) {
    }

    public async create(): Promise<UserModel> {
        const model = new UserModel();
        model.name = capitalizeFirstLetter(
            generateRandomString(10)
                .toLowerCase()
        );
        model.token = generateRandomString(32);

        return this.usersRepository.add(model);
    }

    async getByToken(token: string) {
        const user = await this.usersRepository.findByToken(token);

        if (user) {
            return user;
        }

        return this.create();
    }
}
