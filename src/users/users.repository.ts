import { AbstractRepository } from "@app/common";
import { DbNamesEnum } from "@common/db";
import { Injectable } from "@nestjs/common";
import { UserModel } from "@app/users/user.model";

@Injectable()
export class UsersRepository extends AbstractRepository {
    readonly dbName: DbNamesEnum = DbNamesEnum.MAIN;
    readonly model: any;
    readonly table: string = 'users';

    public async findByToken(token: string): Promise<UserModel> {
        return this.getPool()
            .executeQuery(
                `SELECT * FROM ${this.table} WHERE token = $1`,
                [token]
            ).then((result) => {
                return result.length ? Object.assign(new UserModel(), result[0]) : null;
            });
    }

    public async add(model: UserModel): Promise<UserModel> {
        return this.getPool()
            .executeQuery(
                `INSERT INTO ${this.table} (name, token) VALUES ($1, $2)` +
                ` RETURNING *`,
                [model.name, model.token]
            ).then((result) => {
                return result.length ? Object.assign(new UserModel(), result[0]) : null;
            });
    }
}
