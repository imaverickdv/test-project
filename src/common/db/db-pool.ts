import { Pool, QueryResult } from 'pg';
import { DbNamesEnum } from './db.enum';
import { Logger } from '@nestjs/common';

export interface DbPoolOptions {
    dialect: string;
    debug?: boolean;
}

export class DbPool {
    constructor(
        private dbName: DbNamesEnum,
        private pool: Pool,
        private logger: Logger,
        private options: DbPoolOptions
    ) {}

    private debug(value: any) {
        if (this.options.debug) {
            this.logger.log(value);
        }
    }

    public async executeQuery(queryText: string, values: any[] = []): Promise<any[]> {
        this.debug(`Query from pool: ${this.dbName}`);
        this.debug(queryText);
        this.debug(values);

        return this.pool.query(queryText, values).then((result: QueryResult) => {
            this.debug(`Executed query, result size: ${result.rows.length}`);
            return result.rows;
        });
    }

    public async end() {
        return this.pool.end();
    }
}