import { DynamicModule, Global, Module } from '@nestjs/common';
import { DbService } from './db.service';
import { ConfigurableModuleClass, OPTIONS_TYPE } from './db.definition';

@Global()
@Module({
    providers: [DbService],
    exports: [DbService]
})
export class DbModule extends ConfigurableModuleClass {
    constructor() {
        super();
    }

    static register(options: typeof OPTIONS_TYPE): DynamicModule {
        return {
            ...super.register(options)
        }
    }
}