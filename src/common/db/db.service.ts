import { Inject, Injectable, Logger, OnApplicationShutdown } from '@nestjs/common';
import { Pool } from 'pg';
import { DbModuleOptions, MODULE_OPTIONS_TOKEN } from './db.definition';
// @ts-ignore
import SqlString from 'sequelize/lib/sql-string';
import { DbPool } from './db-pool';
import { DbNamesEnum } from "@common/db/db.enum";
import { DbError } from "@common/errors/db.error";

@Injectable()
export class DbService implements OnApplicationShutdown {
    public static POSTGRES_DIALECT = 'postgres';
    private static dialect: string

    private readonly logger = new Logger(DbService.name);

    private dbPools: Record<string, DbPool> = {};

    constructor(
        @Inject(MODULE_OPTIONS_TOKEN) private options: DbModuleOptions,
    ) {
        for (const connection of options.connections) {
            this.dbPools[connection.name] = new DbPool(
                connection.name,
                new Pool({
                    connectionString: connection.options.connectionUrl
                }),
                this.logger,
                {
                    dialect: connection.options.dialect,
                    debug: this.options.debug,
                })
        }
    }

    public getPool(name: DbNamesEnum): DbPool {
        if (!this.dbPools[name]) {
            throw new DbError(`Pool with name "${name}" not found`);
        }

        return this.dbPools[name];
    }

    static escapeValue(value: any, dialect?: string) {
        return SqlString.escape(value, '', dialect ?? DbService.dialect);
    }

    public onApplicationShutdown(signal?: string): any {
        this.logger.log(`Shutting down on signal ${signal}`);
        for (const poolName in this.dbPools) {
            if (this.dbPools.hasOwnProperty(poolName)) {
                const dbPool = this.dbPools[poolName];
                dbPool.end();
            }
        }
    }
}
