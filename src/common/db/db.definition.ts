import { ConfigurableModuleBuilder } from '@nestjs/common';
import { DbNamesEnum } from './db.enum';

export interface DbModuleOptions {
    connections: DbConnection[];
    debug?: boolean,
    defaultDialect?: string,
}

export interface DbConnection {
    name: DbNamesEnum;
    options: {
        connectionUrl: string,
        dialect: string,
        cachePrefix: string,
    };
}

export const { ConfigurableModuleClass, MODULE_OPTIONS_TOKEN, OPTIONS_TYPE } =
    new ConfigurableModuleBuilder<DbModuleOptions>({
        moduleName: 'Db'
    }).build();