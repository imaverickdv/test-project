import { Injectable } from '@nestjs/common';
import { DbNamesEnum, DbService, DbPool } from '@app/common/db';

@Injectable()
export abstract class AbstractRepository {
    public readonly abstract dbName: DbNamesEnum;
    public readonly abstract model: any;
    public readonly abstract table: string;

    public constructor(
        private dbService: DbService
    ) {}

    protected getPool(): DbPool {
        return this.dbService.getPool(this.dbName);
    }

    protected escape(value: any): any {
        if (Array.isArray(value)) {
            return value.map(
                item => DbService.escapeValue(
                    item,
                    DbService.POSTGRES_DIALECT
                )
            );
        }

        return DbService.escapeValue(value, DbService.POSTGRES_DIALECT);
    }
}