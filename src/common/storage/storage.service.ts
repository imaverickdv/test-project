import { Inject, Injectable, Logger } from '@nestjs/common';
import Redis from 'ioredis';
import { MODULE_OPTIONS_TOKEN, StorageModuleOptions } from './storage.definition';
import { UserModel } from "@app/users/user.model";
import { ClientDataDto } from "@common/ws/dto/client-data.dto";

@Injectable()
export class StorageService {
    private CLIENTS_KEY = 'clients';
    private logger = new Logger(StorageService.name);

    readonly client: Redis;
    private debug = false;

    constructor(
        @Inject(MODULE_OPTIONS_TOKEN) private options: StorageModuleOptions,
    ) {
        this.client = new Redis(options.url);
        this.debug = options.debug ?? false;
    }

    public async connectUser(socketId: string, user: UserModel) {
        return this.client
            .hset(this.CLIENTS_KEY, socketId, JSON.stringify(
                Object.assign(user, {socketId})
            ));
    }

    public async disconnectUser(socketId: string) {
        return this.client.hdel(this.CLIENTS_KEY, socketId);
    }

    public async getConnectedUsers(): Promise<ClientDataDto[]> {
        const clients = await this.client.hvals(this.CLIENTS_KEY);

        if (!clients) {
            return [];
        }

        return clients
            .filter((client: string) => client.length)
            .map((client: string) => JSON.parse(client));
    }

    public async getBySocketId(socketId: string): Promise<ClientDataDto[]> {
        const client = await this.client.hget(this.CLIENTS_KEY, socketId);

        if (!client) {
            throw new Error(`Client with socketId ${socketId} not found`);
        }

        return JSON.parse(client);
    }

    public async clear() {
        return this.client.del(this.CLIENTS_KEY);
    }
}
