import { ConfigurableModuleBuilder } from '@nestjs/common';

export interface StorageModuleOptions {
    url: string,
    prefix: string,
    debug?: boolean
}

export const { ConfigurableModuleClass, MODULE_OPTIONS_TOKEN, OPTIONS_TYPE } =
    new ConfigurableModuleBuilder<StorageModuleOptions>({
        moduleName: 'Storage'
    }).build();