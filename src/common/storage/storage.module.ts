import { DynamicModule, Global, Module } from '@nestjs/common';
import { StorageService } from './storage.service';
import { ConfigurableModuleClass, OPTIONS_TYPE } from './storage.definition';

@Global()
@Module({
    providers: [StorageService],
    exports: [StorageService]
})
export class StorageModule extends ConfigurableModuleClass {
    static register(options: typeof OPTIONS_TYPE): DynamicModule {
        return {
            ...super.register(options)
        }
    }
}
