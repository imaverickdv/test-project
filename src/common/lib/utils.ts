import crypto from 'crypto';

export function generateRandomString(length: number) {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';

    // Генерируем случайную букву верхнего или нижнего регистра
    const randomLetterIndex = Math.floor(Math.random() * 52);
    const randomLetter = characters.charAt(randomLetterIndex);

    result += randomLetter;

    // Генерируем оставшиеся символы
    for (let i = 1; i < length; i++) {
        const randomIndex = Math.floor(Math.random() * characters.length);
        result += characters.charAt(randomIndex);
    }

    return result;
}

export async function sleep(millis: number) {
    return new Promise(resolve => setTimeout(resolve, millis));
}

export function makeHash(str: string) {
    return crypto.createHash('md5')
        .update(str)
        .digest('hex');
}

export function capitalizeFirstLetter(str: string) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}