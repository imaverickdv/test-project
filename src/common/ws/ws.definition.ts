import { ConfigurableModuleBuilder } from '@nestjs/common';

export interface WsModuleOptions {
}

export const { ConfigurableModuleClass, MODULE_OPTIONS_TOKEN, OPTIONS_TYPE } =
    new ConfigurableModuleBuilder<WsModuleOptions>({
        moduleName: 'WS'
    }).build();