import { Socket } from 'socket.io';

export function HandleMessage() {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        const originalMethod = descriptor.value;

        descriptor.value = async function (this: any, client: Socket, ...args: any[]) {
            const payload = args[0];

            console.log(`[${client.id}] - Income message: ${propertyKey}`, payload);

            try {
                return await originalMethod.call(this, client, ...args);
            } catch (error: any) {
                console.error(`[${client.id}] - Error in method ${propertyKey}:`, error);

                client.emit('Error', error.message);
            }
        };

        return descriptor;
    };
}