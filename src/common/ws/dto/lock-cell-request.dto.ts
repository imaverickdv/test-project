export class LockCellRequestDto {
    public cellId: string;
    public userId: string;
}