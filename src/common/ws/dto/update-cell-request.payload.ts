export class UpdateCellRequestPayload {
    public cellId: string;
    public value: string;
}