import { UserModel } from "@app/users/user.model";

export class ClientDataDto {
    public socketId: string;
    public user: UserModel;
}