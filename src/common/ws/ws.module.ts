import { DynamicModule, Global, Module } from '@nestjs/common';
import { WsGateway } from './ws.gateway';
import { WsService } from './ws.service';
import { ConfigurableModuleClass, OPTIONS_TYPE } from './ws.definition';

@Global()
@Module({
    providers: [WsGateway, WsService],
    exports: [WsGateway, WsService]
})
export class WsModule extends ConfigurableModuleClass {
    static register(options: typeof OPTIONS_TYPE): DynamicModule {
        return {
            ...super.register(options)
        }
    }
}
