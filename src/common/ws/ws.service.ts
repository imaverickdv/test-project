import { Inject, Injectable } from '@nestjs/common';
import { MODULE_OPTIONS_TOKEN, WsModuleOptions } from '@common/ws/ws.definition';

@Injectable()
export class WsService {
    constructor(
        @Inject(MODULE_OPTIONS_TOKEN) private options: WsModuleOptions,
    ) {}
}