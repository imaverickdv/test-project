import { Inject, Logger } from "@nestjs/common";
import { MODULE_OPTIONS_TOKEN, WsModuleOptions } from "@common/ws/ws.definition";
import { Server, Socket } from "socket.io";

import {
    WebSocketGateway,
    WebSocketServer,
    SubscribeMessage,
    ConnectedSocket,
    MessageBody,
    OnGatewayConnection,
    OnGatewayDisconnect
} from "@nestjs/websockets";
import { UsersService } from "@app/users";
import { StorageService } from "@common/storage";
import { GridService } from "@app/grid/grid.service";
import { UpdateCellRequestPayload } from "@common/ws/dto/update-cell-request.payload";
import { LockCellRequestDto } from "@common/ws/dto/lock-cell-request.dto";

@WebSocketGateway({namespace: "socket"})
export class WsGateway implements OnGatewayConnection, OnGatewayDisconnect {
    private logger = new Logger(WsGateway.name);

    constructor(
        @Inject(MODULE_OPTIONS_TOKEN) private options: WsModuleOptions,
        private usersService: UsersService,
        private gridService: GridService,
        private storage: StorageService
    ) {
    }

    @WebSocketServer()
    server: Server;

    async handleConnection(client: Socket) {
        console.log(`New client connected: ${client.id}`);
        const token = client.handshake.auth.token;
        const user = await this.usersService.getByToken(token);

        await this.storage.connectUser(client.id, user);

        client.data = {user};
        client.emit("connection", user);

        this.server.emit("user-connected", user);
    }

    async handleDisconnect(client: Socket) {
        console.log(`Client disconnected: ${client.id}`);
        await this.storage.disconnectUser(client.id);

        this.server.emit("user-disconnected", client.data.user);
    }

    @SubscribeMessage("update-cell-value")
    public async handleUpdateCellValue(
        @ConnectedSocket() client: Socket,
        @MessageBody() payload: UpdateCellRequestPayload
    ) {
        const cellId = parseInt(payload.cellId.replace('cell-', ''));
        const value = parseInt(payload.value);

        if (isNaN(cellId) || isNaN(value)) {
            throw new Error("Invalid cellId or value");
        }

        await this.gridService.updateCell(client.data.user.id, cellId, value);

        this.server.emit("cell-updated", {cellId, value});
    }

    @SubscribeMessage("lock-cell")
    public async handleLockCell(
        @ConnectedSocket() client: Socket,
        @MessageBody() payload: LockCellRequestDto
    ) {
        const cellId = parseInt(payload.cellId);
        const lockUserId = parseInt(payload.userId);

        if (isNaN(cellId) || isNaN(lockUserId)) {
            throw new Error("Invalid cellId or userId");
        }

        await this.gridService.lockCell(cellId, client.data.user.id, lockUserId);

        this.server.emit("cell-locked", {
            cellId,
            userId: lockUserId
        });
    }

    @SubscribeMessage("unlock-cell")
    public async handleUnlockCell(
        @ConnectedSocket() client: Socket,
        @MessageBody() payload: any
    ) {
        const cellId = parseInt(payload.cellId);

        if (isNaN(cellId)) {
            throw new Error("Invalid cellId");
        }

        await this.gridService.unlockCell(cellId, client.data.user.id);

        this.server.emit("cell-unlocked", {
            cellId
        });
    }
}