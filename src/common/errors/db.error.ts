export class DbError extends Error {
    constructor(message = 'DB error') {
        super(message);
    }
}