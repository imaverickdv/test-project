import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { ConfigModule } from '@nestjs/config';

import { DbModule, DbService } from '@app/common/db';
import { DbNamesEnum } from '@app/common/db';
import { WsModule } from '@common/ws';
import { StorageModule } from "@common/storage";
import { UsersModule } from "@app/users";
import { GridModule } from "@app/grid/grid.module";
import { AppService } from "@app/app.service";
import { ScheduleModule } from "@nestjs/schedule";

export const PREFIX = 'main';

@Module({
    controllers: [AppController],
    providers: [AppService],
    imports: [
        ScheduleModule.forRoot(),
        ConfigModule.forRoot({
            isGlobal: true,
            envFilePath: ['.env'],
        }),
        StorageModule.register({
            debug: true,
            url: process.env.REDIS_URL ?? '',
            prefix: PREFIX,
        }),
        WsModule.register({}),
        DbModule.register({
            debug: true,
            defaultDialect: DbService.POSTGRES_DIALECT,
            connections: [
                {
                    name: DbNamesEnum.MAIN,
                    options: {
                        connectionUrl: process.env.POSTGRES_URL as string,
                        dialect: DbService.POSTGRES_DIALECT,
                        cachePrefix: DbNamesEnum.MAIN,
                    }
                },
            ]
        }),
        UsersModule.register({}),
        GridModule.register({}),
    ]
})
export class AppModule {
}
