import { Injectable } from '@nestjs/common';
import { StorageService } from "@common/storage";
import { ClientDataDto } from "@common/ws/dto/client-data.dto";
import { Timeout } from "@nestjs/schedule";

@Injectable()
export class AppService {
    constructor(
        private storageService: StorageService,
    ) {}

    @Timeout(1)
    private async initialize() {
        await this.storageService.clear();
    }

    public async getOnlineUsers(): Promise<ClientDataDto[]> {
        return this.storageService.getConnectedUsers();
    }
}
