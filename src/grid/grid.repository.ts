import { AbstractRepository } from "@app/common";
import { DbNamesEnum } from "@app/common/db";
import { GridModel } from "@app/grid/grid.model";
import { UserModel } from "@app/users/user.model";

export class GridRepository extends AbstractRepository {
    public dbName: DbNamesEnum = DbNamesEnum.MAIN
    public model: GridModel;
    public table: string = 'grid';

    async save(table: {col: number; row: number; value: number}[]) {
        return this.getPool()
            .executeQuery(
                `INSERT INTO ${this.table} (row, col, value) VALUES ` +
                table.map((row) => `(${row.row}, ${row.col}, ${row.value})`).join(', ')
            );
    }

    public async getAll(): Promise<GridModel[]> {
        return this.getPool()
            .executeQuery(`SELECT * FROM ${this.table}`)
            .then((result) => result.map((row) => Object.assign(
                new GridModel(),
                row,
                { userId: row.user_id }
            )));
    }

    public async getAvailableCell(id: number, userId: number): Promise<GridModel> {
        return this.getPool()
            .executeQuery(`SELECT * FROM ${this.table}
                WHERE id = $1 AND (user_id = $2 OR user_id IS NULL)`, [id, userId])
            .then((result) => {
                return result.length ? Object.assign(new GridModel(), result[0]) : null;
            });
    }

    public async update(cellId: number, value: number) {
        return this.getPool()
            .executeQuery(
                `UPDATE ${this.table} SET value = $1 WHERE id = $2`,
                [value, cellId]
            );
    }

    public async lockCell(id: number, userId: number) {
        return this.getPool()
            .executeQuery(
                `UPDATE ${this.table} SET user_id = $1 WHERE id = $2`,
                [userId, id]
            );
    }

    public async unlockCell(id: number) {
        return this.getPool()
            .executeQuery(
                `UPDATE ${this.table} SET user_id = NULL WHERE id = $1`,
                [id]
            );
    }
}