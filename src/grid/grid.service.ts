import { Injectable } from "@nestjs/common";
import { GridRepository } from "@app/grid/grid.repository";
import { Timeout } from "@nestjs/schedule";

@Injectable()
export class GridService {
    constructor(
        private gridRepository: GridRepository
    ) {
    }

    // @Timeout(1)
    private async initialize() {
        const grid = [];

        for (let i = 0; i < 100; i++) {
            for (let j = 0; j < 100; j++) {
                grid.push({ row: i, col: j, value: Math.floor(Math.random() * 10000) });
            }
        }

        await this.gridRepository.save(grid);
    }

    public async getGrid() {
        const grid = await this.gridRepository.getAll();
        let table = '<table id="grid" class="table">';

        for (let i = 0; i < 100; i++) {
            table += '<tr>';
            for (let j = 0; j < 100; j++) {
                const cell = grid.find((cell) => cell.row === i && cell.col === j);
                table += `<td id="cell-${cell!.id}"
                    data-id="${cell!.id}"
                    data-user-id="${cell!.userId}"
                    contenteditable="true"
                    onblur="updateCellValue('cell-${cell!.id}')"
                    oncontextmenu="showContextMenu(event, 'cell-${cell!.id}')">${cell!.value}</td>`;
            }
            table += '</tr>';
        }

        return `${table}</table>`;
    }

    private async checkCell(userId: number, cellId: number) {
        const availableCell = await this.gridRepository.getAvailableCell(cellId, userId);

        if (!availableCell) {
            throw new Error("Cell is unknown or unavailable");
        }
    }

    public async updateCell(userId: number, cellId: number, value: number): Promise<void> {
        await this.checkCell(userId, cellId);
        await this.gridRepository.update(cellId, value);
    }

    public async lockCell(cellId: number, userId: number, lockUserId: number) {
        await this.checkCell(userId, cellId);
        await this.gridRepository.lockCell(cellId, lockUserId);
    }

    public async unlockCell(cellId: number, userId: number) {
        await this.checkCell(userId, cellId)
        await this.gridRepository.unlockCell(cellId);
    }
}