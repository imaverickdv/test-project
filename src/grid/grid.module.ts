import { DynamicModule, Global, Module } from "@nestjs/common";
import { GridService } from "@app/grid/grid.service";
import { GridRepository } from "@app/grid/grid.repository";
import { ConfigurableModuleClass, OPTIONS_TYPE } from "@app/grid/grid.definition";

@Global()
@Module({
    providers: [GridService, GridRepository],
    exports: [GridService]
})
export class GridModule extends ConfigurableModuleClass {
    static register(options: typeof OPTIONS_TYPE): DynamicModule {
        return {
            ...super.register(options)
        }
    }
}
