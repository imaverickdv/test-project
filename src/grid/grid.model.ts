import { AbstractModel } from "@app/common";

export class GridModel extends AbstractModel {
    public id: number;
    public value: number;
    public row: number;
    public col: number;
    public userId?: number;
}