import { ConfigurableModuleBuilder } from '@nestjs/common';

export interface GridModuleOptions {
    debug?: boolean;
}

export const { ConfigurableModuleClass, MODULE_OPTIONS_TOKEN, OPTIONS_TYPE } =
    new ConfigurableModuleBuilder<GridModuleOptions>({
        moduleName: 'Grid'
    }).build();