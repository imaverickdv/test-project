import 'module-alias/register';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { useContainer } from 'class-validator';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';
import * as hbsHelpers from '@app/view-helpers/hbs.helpers';
import hbs from 'express-handlebars';
import { IoAdapter } from '@nestjs/platform-socket.io';

async function bootstrap() {
    const app = await NestFactory.create<NestExpressApplication>(AppModule);

    app.enableCors({
        origin: '*',
        // origin: 'http://127.0.0.1:3300',
        methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
        credentials: true,
    });

    app.useWebSocketAdapter(new IoAdapter(app));

    app.setBaseViewsDir(join(__dirname, '..', 'views'));
    app.engine('hbs', hbs({
        extname: 'hbs',
        layoutsDir: join(__dirname, '..', 'views/layouts'),
        defaultLayout: 'main',
        helpers: { ...hbsHelpers }
    }));
    app.setViewEngine('hbs');

    useContainer(app.select(AppModule), { fallbackOnErrors: true });

    await app.listen(process.env.SERVER_PORT as string, () => {
        console.log(`listening on: ${process.env.SERVER_PORT}`);
    });
}

bootstrap();
