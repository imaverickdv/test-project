import { Controller, Get, Post, Res } from '@nestjs/common';
import { AbstractController } from '@app/common';
import { Response } from 'express';
import { AppService } from "@app/app.service";
import { GridService } from "@app/grid/grid.service";

@Controller()
export class AppController extends AbstractController {
    constructor(
        private appService: AppService,
        private gridService: GridService,
    ) {
        super()
    }

    @Get('/')
    public async index(@Res() res: Response) {
        return res.render('index', {
            title: 'Test Project',
            grid: await this.gridService.getGrid(),
            users: await this.appService.getOnlineUsers(),
        });
    }
}
