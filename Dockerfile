FROM node:20-alpine

ENV NODE_ENV=dev

WORKDIR /test

COPY ["package.json", "package-lock.json", "/test/"]

RUN npm install

COPY . .

CMD ["npm", "run", "start:dev"]